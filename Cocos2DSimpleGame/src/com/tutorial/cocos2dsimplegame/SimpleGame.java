package com.tutorial.cocos2dsimplegame;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.opengl.CCGLSurfaceView;

import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.util.Log;
import android.view.Menu;
//import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;
//import android.support.v4.app.NavUtils;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;


public class SimpleGame extends Activity implements SensorEventListener {
	protected CCGLSurfaceView _glSurfaceView;
	private SensorManager sensorManager;
	public static int threshold=10;//Accelerometer doesn't read 0 when its not moving.
	public static int ythreshold=9;
	public static int runs=1;
	public static float sumx=0;
	public static float sumy=0;
	public static float sumz=0;
	public static float realx=0;
	public static float realy=0;
	public static float realz=0;
	protected static float deltax;
	protected static float deltay;
	protected static float deltaz;
	protected static float olddx;
	protected static float olddy;
	protected static float olddz;
	protected static float realdx;
	protected static float realdy;
	protected static float realdz;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        // This sets up the OpenGL surface for Cocos2D to utilize.
        // We set some flags to ensure we always have a full screen view, 
        // then display the view to the user.
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON, WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
		
        _glSurfaceView = new CCGLSurfaceView(this);
        
		sensorManager=(SensorManager)getSystemService(SENSOR_SERVICE); // add listener. The listener will be Exercise (this) class
		sensorManager.registerListener(this,
				sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
				SensorManager.SENSOR_DELAY_NORMAL);
     
        setContentView(_glSurfaceView);
        
        runs = new File("/mnt/sdcard/Download").listFiles().length;//set run number for book-keeping
   
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_simple_game, menu);
        return true;
    }
    
    @Override
    public void onStart()
    {
        super.onStart();
        /* First we tell Cocos2D which surface to render to (the OpenGL surface we set up earlier). 
         * We then ask Cocos2D to display the FPS and to run at 60fps. 
         */
        CCDirector.sharedDirector().attachInView(_glSurfaceView);
     
        CCDirector.sharedDirector().setDisplayFPS(true);
     
        CCDirector.sharedDirector().setAnimationInterval(1.0f / 60.0f);
        
        CCScene scene = GameLayer.scene();
        
        CCDirector.sharedDirector().runWithScene(scene);
    }
    
    /* These notify Cocos2D with what's going on with the device such as when the user has switched to another application or the game is being stopped by the OS.*/
     
    @Override
    public void onPause()
    {
        super.onPause();
     
        CCDirector.sharedDirector().pause();
    }
     
    @Override
    public void onResume()
    {
        super.onResume();
     
        CCDirector.sharedDirector().resume();
    }
     
    @Override
    public void onStop()
    {
        super.onStop();
     
        CCDirector.sharedDirector().end();
    }
    
    public void onAccuracyChanged(Sensor sensor,int accuracy){
		//Even though this method is empty it is required for implementing sensorEvent listener
	}

	public void onSensorChanged(SensorEvent event){
			
		// check sensor type
		if(event.sensor.getType()==Sensor.TYPE_ACCELEROMETER){

			// assign directions
			float deltax=event.values[0];
			float deltay=event.values[1];
			float deltaz=event.values[2];
			
			float deltax1=event.values[0];
			float deltay1=event.values[1];
			float deltaz1=event.values[2];
			
			float deltax2=event.values[0];
			float deltay2=event.values[1];
			float deltaz2=event.values[2];
			
			float deltax3=event.values[0];
			float deltay3=event.values[1];
			float deltaz3=event.values[2];
			
			//Moving Average
			float smoothx = (deltax+ deltax1 + deltax2 +deltax3)/4.0f;
			float smoothy = (deltay+ deltay1 + deltay2 +deltay3)/4.0f;
			float smoothz = (deltaz+ deltaz1 + deltaz2 +deltaz3)/4.0f;
			
			updateRawCount(smoothx,smoothy,smoothz);
			
		}
	}
	
	public void updateRawCount(float smoothx, float smoothy, float smoothz){
		//make sure the change was valid.
		if (smoothx>threshold){
			realdx=smoothx*smoothx;
			sumx=sumx+smoothx-threshold;
			realx=smoothx-threshold;
		}
		else
			realdx= (float) (smoothx*0.90);//Horizontal Gravity
		
		if (smoothy>ythreshold){
			realdy=smoothy*smoothy;
			sumy=sumy+smoothy-ythreshold;
			realy=smoothy-ythreshold;

		}
		else
			realdy=(float) (realdy*0.90);//Vertical Gravity
		
		if (smoothz>threshold){
			realdz=smoothz;
			sumz=sumz+smoothz-threshold;
			realz=smoothz-threshold;
			}
		
		realdz=smoothz;//making smoothz effectively public. Temporary solution.
		
		//Quantities being written to SD card
	    File path1 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS);
	    File path2 = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_ALARMS);
	    File rawfile = new File(path1,  "smoothgame" + runs + ".csv");
	    File filtfile = new File(path2, "threshgame" + runs + ".csv");
		
	    createFile(rawfile,path1, smoothx,smoothy,smoothz);//Write smooth but raw output to SD card
		createFile(filtfile,path2, realx,realy, realz);//Write smooth + threshold output to SD card
		
		GameLayer.movePlayer(realdx,realdy,Math.abs(deltaz));
	}

	static public float accelx(){
		
		return realdx;
		
	}
	
	static public float accely(){
		
		return realdy;
		
	}
	
	static public float getz(){
		
		return realdz;
		
	}
	
	static public void setz(float val){
		
		realdz=val;
		
	}
	
	public void createFile(File file, File path, float deltax, float deltay, float deltaz) {
		// Create a path where we will place our file in the user's
	    // public pictures directory. For pictures and other media 
		//owned by the application, consider Context.getExternalMediaDir().
		//This function was based of the Android developer documentation example.

	    try {
	        // Make sure the directory exists.
	        path.mkdirs();

	        // Note that this code does no error checking, and assumes the data is small (does not
	        // try to copy it in chunks).  Note that if external storage is not currently mounted this will silently fail.
	        OutputStream os = new FileOutputStream(file, true);
	        String stringdata = deltax + "," + deltay + "," + deltaz + "\r";
	        byte[] bytedata = new byte[stringdata.length()];
	        bytedata = stringdata.getBytes();
	        os.write(bytedata);
	        os.close();

	        // Tell the media scanner about the new file so that it is immediately available to the user.
	        MediaScannerConnection.scanFile(this,
	                new String[] { file.toString() }, null,
	                new MediaScannerConnection.OnScanCompletedListener() {
	            public void onScanCompleted(String path, Uri uri) {
	                Log.i("ExternalStorage", "Scanned " + path + ":");
	                Log.i("ExternalStorage", "-> uri=" + uri);
	            }


	        });
	    } catch (IOException e) {
	        // Unable to create file, likely because external storage is
	        // not currently mounted.
	        Log.w("ExternalStorage", "Error writing " + file, e);
	    }
	}

    
}
