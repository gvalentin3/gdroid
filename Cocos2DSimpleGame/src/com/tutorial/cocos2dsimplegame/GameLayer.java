package com.tutorial.cocos2dsimplegame;

import java.io.File;
import java.util.ArrayList;
import java.util.Random;

import org.cocos2d.actions.instant.CCCallFunc;
import org.cocos2d.actions.instant.CCCallFuncN;
import org.cocos2d.actions.interval.CCMoveTo;
import org.cocos2d.actions.interval.CCRotateTo;
import org.cocos2d.actions.interval.CCSequence;
import org.cocos2d.layers.CCColorLayer;
import org.cocos2d.layers.CCScene;
import org.cocos2d.nodes.CCDirector;
import org.cocos2d.nodes.CCSprite;
import org.cocos2d.sound.SoundEngine;
import org.cocos2d.types.CGPoint;
import org.cocos2d.types.CGRect;
import org.cocos2d.types.CGSize;
import org.cocos2d.types.ccColor4B;

import android.content.Context;
import android.os.Environment;
import android.util.FloatMath;
import android.view.MotionEvent;

public class GameLayer extends CCColorLayer {
	protected ArrayList<CCSprite> _targets;
	protected static ArrayList<CCSprite> _projectiles;
	protected static CCSprite _player;
	protected CCSprite _nextProjectile;
	protected int _projectilesDestroyed;
	
	public static CCScene scene()
	{
	    CCScene scene = CCScene.node();
	    // CCLayer layer = new GameLayer();
	    CCColorLayer layer = new GameLayer(ccColor4B.ccc4(255, 255, 255, 255));
	 
	    scene.addChild(layer);
	 
	    return scene;
	}
	
	protected GameLayer(ccColor4B color)
	{	
		//Default constructor:
		super(color);
	    this.setIsTouchEnabled(true);
	    
		_targets = new ArrayList<CCSprite>();
		_projectiles = new ArrayList<CCSprite>();
		_projectilesDestroyed = 0;

	    CGSize winSize = CCDirector.sharedDirector().displaySize();
	    //CSprite player = CCSprite.sprite("Player.png");
	    
	    _player = CCSprite.sprite("Player2.png");
	    //player.setPosition(CGPoint.ccp(player.getContentSize().width / 2.0f, winSize.height / 2.0f)); // (Tutorial 1)
	    _player.setPosition(CGPoint.ccp(_player.getContentSize().width / 2.0f, _player.getContentSize().height/2.0f)); // (Tutorial 1)
	 
	    //this.addChild(player);//appending "this" fixed error //but now taking it away is ok. (Tutorial 1)
	    addChild(_player); //(Tutorial 2)
	    
	    this.schedule("gameLogic", 1.0f);//nose que hace esto
	    this.schedule("update");//last line
	    

	}
	
	protected void addTarget()
	{
	    Random rand = new Random();
	    CCSprite target = CCSprite.sprite("Target.png");
	 
	    // Determine where to spawn the target along the Y axis
	    CGSize winSize = CCDirector.sharedDirector().displaySize();
	    int minY = (int)(target.getContentSize().height / 2.0f);
	    int maxY = (int)(winSize.height - target.getContentSize().height / 2.0f);
	    int rangeY = maxY - minY;
	    //int actualY = rand.nextInt(rangeY) + minY;
	    int actualY= minY;
	    
	    // Create the target slightly off-screen along the right edge,
	    // and along a random position along the Y axis as calculated above
	    //target.setPosition(winSize.width + (target.getContentSize().width / 2.0f), actualY); //gave error used alternative below
	    
	    target.setPosition(CGPoint.ccp(winSize.width + (target.getContentSize().width / 2.0f) , actualY/ 1.0f));//correction based on SO.
	    addChild(target);
	    
	    target.setTag(1);
	    _targets.add(target);
	 
	    // Determine speed of the target
	    int minDuration = 4;
	    int maxDuration = 8;
	    int rangeDuration = maxDuration - minDuration;
	    int actualDuration = rand.nextInt(rangeDuration) + minDuration;
	    actualDuration = 9;
	    // Create the actions
	    
	    		CCMoveTo actionMove = CCMoveTo.action(actualDuration, CGPoint.ccp(-target.getContentSize().width / 2.0f, actualY));
	    		CCCallFuncN actionMoveDone = CCCallFuncN.action(this, "spriteMoveFinished");
	    		CCSequence actions = CCSequence.actions(actionMove, actionMoveDone);
	    		target.runAction(actions);
	    	
	    	
	}
	
	protected void addSuperTarget()
	{
	    Random rand = new Random();
	    CCSprite target = CCSprite.sprite("Projectile.png");
	 
	    // Determine where to spawn the target along the Y axis
	    CGSize winSize = CCDirector.sharedDirector().displaySize();
	    int minY = 30;
	    int maxY = (int)(winSize.height - target.getContentSize().height / 2.0f);
	    int rangeY = maxY - minY;
	    //int actualY = rand.nextInt(rangeY) + minY;
	    int actualY= minY;
	    
	    // Create the target slightly off-screen along the right edge,
	    // and along a random position along the Y axis as calculated above
	    //target.setPosition(winSize.width + (target.getContentSize().width / 2.0f), actualY); //gave error used alternative below
	    
	    target.setPosition(CGPoint.ccp(winSize.width + (target.getContentSize().width / 2.0f) , actualY/ 1.0f));//correction based on SO.
	    addChild(target);
	    
	    target.setTag(1);
	    _targets.add(target);
	 
	    // Determine speed of the target
	    int minDuration = 2;
	    int maxDuration = 8;
	    int rangeDuration = maxDuration - minDuration;
	    int actualDuration = rand.nextInt(rangeDuration) + minDuration;
	    actualDuration = 9;
	    // Create the actions
	    
	    		CCMoveTo actionMove = CCMoveTo.action(actualDuration, CGPoint.ccp(-target.getContentSize().width / 2.0f, actualY));
	    		CCCallFuncN actionMoveDone = CCCallFuncN.action(this, "spriteMoveFinished");
	    		CCSequence actions = CCSequence.actions(actionMove, actionMoveDone);
	    		target.runAction(actions);
	    	
	    	
	}
	
	public void spriteMoveFinished(Object sender)
	{
	    CCSprite sprite = (CCSprite)sender;
	    this.removeChild(sprite, true);
	    
	    if (sprite.getTag() == 1)
	    {
	        _targets.remove(sprite);
	     
	        _projectilesDestroyed = 0;
	        //CCDirector.sharedDirector().replaceScene(GameOverLayer.scene("You Lose, boo"));
	    }
	    else if (sprite.getTag() == 2)
	        _projectiles.remove(sprite);
	    
	    /* Old
	    if (sprite.getTag() == 1)
	        _targets.remove(sprite);
	    else if (sprite.getTag() == 2)
	        _projectiles.remove(sprite);
	        */
	}
	
	public void gameLogic(float dt)
	{
		
		if (Math.random()<0.20)//originally .3	
			addTarget();
		else if (Math.random()>0.8)
			addSuperTarget();
	    	
	    if (Math.abs(SimpleGame.getz())>=9.00){//9.893
	    	startShoot();
	    	//SimpleGame.setz(0);
	    }
	}
	
	@Override
	public boolean ccTouchesEnded(MotionEvent event)
	{
		startShoot();
	    return true;
	    
	}
	
	public static void movePlayer(float realdx,float realdy,float realdz)
	{

		//CGPoint point = CGPoint.ccp(_player.getContentSize().width / 2.0f, _player.getContentSize().height/2.0f + realdy*realdy);
		float x = _player.getContentSize().width / 2.0f + realdx; //not quite working yet
		float y =  _player.getContentSize().height/2.0f + realdy; //not quite working yet
		CGPoint point = CGPoint.ccp(x,y);
	 	CCMoveTo actionMove = CCMoveTo.action(0, point);
	 	_player.runAction(actionMove);
	}
	
	public void startShoot()
	{
	    // Choose one of the touches to work with
	    //CGPoint location = CCDirector.sharedDirector().convertToGL(CGPoint.ccp(event.getX(), event.getY()));
		CGPoint location =  CCDirector.sharedDirector().convertToGL(CGPoint.ccp(450,733)); //hard-coded since shooting is no longer touch based.
		//CGPoint location =  CCDirector.sharedDirector().convertToGL(addTarge, Target.getPosition().y);
		
	    // Set up initial location of projectile
	    CGSize winSize = CCDirector.sharedDirector().displaySize();
	    CCSprite projectile = CCSprite.sprite("Projectile.png");
	    
	    projectile.setPosition(_player.getPosition()); //arreglado

	    // Determine offset of location to projectile
	    int offX = (int)(location.x - projectile.getPosition().x);
	    int offY = (int)(location.y - projectile.getPosition().y);
	 
	    // Bail out if we are shooting down or backwards
	    //if (offX <= 0)
	    //    return true;
	 
	    // Ok to add now - we've double checked position
	    addChild(projectile);
	    projectile.setTag(2);
	    _projectiles.add(projectile);
	 
	    // Determine where we wish to shoot the projectile to
	    int realX = (int)(winSize.width + (projectile.getContentSize().width / 2.0f));
	    float ratio = (float)offY / (float)offX;
	    int realY = (int)((realX * ratio) + projectile.getPosition().y);
	    CGPoint realDest = CGPoint.ccp(realX, realY);
	 
	    // Determine the length of how far we're shooting
	    int offRealX = (int)(realX - projectile.getPosition().x);
	    int offRealY = (int)(realY - projectile.getPosition().y);
	    float length = (float)Math.sqrt((offRealX * offRealX) + (offRealY * offRealY));
	    float velocity = 680.0f / 1.0f; // 480 pixels / 1 sec
	    float realMoveDuration = length / velocity;
	 
	    // Move projectile to actual endpoint
	    projectile.runAction(CCSequence.actions(
	    CCMoveTo.action(realMoveDuration, realDest),
	    CCCallFuncN.action(this, "spriteMoveFinished")));
	    
	    //Background Music
	 	Context context = CCDirector.sharedDirector().getActivity();
	 	SoundEngine.sharedEngine().preloadEffect(context, R.raw.pew_pew_lei);
	 	SoundEngine.sharedEngine().playSound(context, R.raw.background_music_aac, true);
	    
	 	// Pew sound
	 	context = CCDirector.sharedDirector().getActivity();
	 	SoundEngine.sharedEngine().playEffect(context, R.raw.pew_pew_lei);
	  
	}
	
	
	public void finishShoot()
	{
	    addChild(_nextProjectile);
	    _projectiles.add(_nextProjectile);
	}
	
	public void update(float dt)
	{
	    ArrayList<CCSprite> projectilesToDelete = new ArrayList<CCSprite>();
	 
	    CGRect playerRect = CGRect.make(
	    		_player.getPosition().x - (_player.getContentSize().width / 2.0f),
                _player.getPosition().y - (_player.getContentSize().height / 2.0f),
                _player.getContentSize().width,
                _player.getContentSize().height);
	    
	    
	    for (CCSprite projectile : _projectiles)
	    {
	        CGRect projectileRect = CGRect.make(projectile.getPosition().x - (projectile.getContentSize().width / 2.0f),
	                                            projectile.getPosition().y - (projectile.getContentSize().height / 2.0f),
	                                            projectile.getContentSize().width+10,
	                                            projectile.getContentSize().height+10);
	 
	        ArrayList<CCSprite> targetsToDelete = new ArrayList<CCSprite>();
	        
	        for (CCSprite target : _targets)
	        {
	            CGRect targetRect = CGRect.make(target.getPosition().x - (target.getContentSize().width),
	                                            target.getPosition().y - (target.getContentSize().height),
	                                            target.getContentSize().width,
	                                            target.getContentSize().height);
	            if (CGRect.intersects(projectileRect, targetRect))
	                targetsToDelete.add(target);

	        }
	        for (CCSprite target : targetsToDelete)
	        {
	            _targets.remove(target);
	            removeChild(target, true);
	        }
	 
	        if (targetsToDelete.size() > 0)
	            projectilesToDelete.add(projectile);
	    }
	    
	 
        for (CCSprite target : _targets)
        {
            CGRect targetRect = CGRect.make(target.getPosition().x - (target.getContentSize().width),
                                            target.getPosition().y - (target.getContentSize().height),
                                            target.getContentSize().width,
                                            target.getContentSize().height);
            
            if (CGRect.intersects(playerRect, targetRect)){
            	CCDirector.sharedDirector().replaceScene(GameOverLayer.scene("You Lost. Try Again!"));
            }
        }
	    
	    
	    for (CCSprite projectile : projectilesToDelete)
	    {
	        _projectiles.remove(projectile);
	        removeChild(projectile, true);
	        
	        if (++_projectilesDestroyed > 4)
	        {
	            _projectilesDestroyed = 0;
	            CCDirector.sharedDirector().replaceScene(GameOverLayer.scene("You Win. Try Again!"));
	        }
	    }
	}

	
}