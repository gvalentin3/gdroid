package exercise.app;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.widget.TextView;

import android.util.Log;
import android.view.Menu;
//import android.view.MenuItem;
//import android.support.v4.app.NavUtils;

public class DisplayMessageActivity extends Activity {
   
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
     // Get the message from the intent
	    Intent intent = getIntent();
	    String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);
	   //float message = intent.getFloatExtra("MainActivity.EXTRA_MESSAGE", MainActivity.burntcalories);

	    // Create the text view
	    TextView foodView = new TextView(this);
	    foodView.setTextSize(40);
	    foodView.setText("You added a " + message + " to your list.");

	    setContentView(foodView);
	    updateFood(10,200);
	    
        //setContentView(R.layout.activity_display_message);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_display_message, menu);
        return true;
    }
    
	public float updateFood(float foodcalories, float newfood){
	
		foodcalories= foodcalories + newfood;
		createExternalStoragePublic(newfood);
	
		return foodcalories;
	
	}
		
	public void createExternalStoragePublic(float newfood) {
			// Create a path where we will place our file in the user's
		    // public pictures directory. For pictures and other media 
			//owned by the application, consider Context.getExternalMediaDir().
		
		    File path = Environment.getExternalStoragePublicDirectory(
		            Environment.DIRECTORY_DOWNLOADS);
		    File file = new File(path, "foodfile.txt");

		    try {
		        // Make sure the directory exists.
		        path.mkdirs();

		        // Note that this code does no error checking, and assumes the data is small (does not
		        // try to copy it in chunks).  Note that if external storage is
		        // not currently mounted this will silently fail.
		        OutputStream os = new FileOutputStream(file);
		        //byte[is.available()];
		        byte[] data = new byte[1024];//Hae Won //a.length();
		        //is.read(data);
		        int x = 16;
		        int y = 32;
		        String a = "X= "+ x + " Y= " + y;
		        data = a.getBytes();
		        os.write(data);
		        //is.close();
		        os.close();

		        // Tell the media scanner about the new file so that it is
		        // immediately available to the user.
		        MediaScannerConnection.scanFile(this,
		                new String[] { file.toString() }, null,
		                new MediaScannerConnection.OnScanCompletedListener() {
		            public void onScanCompleted(String path, Uri uri) {
		                Log.i("ExternalStorage", "Scanned " + path + ":");
		                Log.i("ExternalStorage", "-> uri=" + uri);
		            }


		        });
		    } catch (IOException e) {
		        // Unable to create file, likely because external storage is
		        // not currently mounted.
		        Log.w("ExternalStorage", "Error writing " + file, e);
		    }
		}

		
	}

